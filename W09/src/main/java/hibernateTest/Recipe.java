package hibernateTest;

import javax.persistence.*;

@Entity
@Table(name = "recipeList", schema = "recipes", catalog = "")
public class Recipe {
    private int idrecipeList;
    private String recipeName;
    private String recipeIngredients;
    private String prepMethod;
    private Integer dayPlanned;

    @Id
    @Column(name = "idrecipeList")
    public int getIdrecipeList() {
        return idrecipeList;
    }

    public void setIdrecipeList(int idrecipeList) {
        this.idrecipeList = idrecipeList;
    }

    @Basic
    @Column(name = "recipeName")
    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    @Basic
    @Column(name = "recipeIngredients")
    public String getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(String recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    @Basic
    @Column(name = "prepMethod")
    public String getPrepMethod() {
        return prepMethod;
    }

    public void setPrepMethod(String prepMethod) {
        this.prepMethod = prepMethod;
    }

    @Basic
    @Column(name = "dayPlanned")
    public Integer getDayPlanned() {
        return dayPlanned;
    }

    public void setDayPlanned(Integer dayPlanned) {
        this.dayPlanned = dayPlanned;
    }

    @Override
    public String toString() {
        return "\nRecipe ID: " + idrecipeList +
                "\nRecipe Name: " + recipeName +
                "\nRecipe Ingreedients: " + recipeIngredients +
                "\nPreparation Method: " + prepMethod +
                "\nDay Planned: " + dayPlanned;
    }
}
