import hibernateTest.Recipe;
import org.hibernate.JDBCException;

import java.util.InputMismatchException;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        Scanner input = new Scanner(System.in);

        int menuChoice;
        boolean repeat;

        try {
          do {
                try {
                    System.out.println("Welcome to the Recipe Database. Please make a choice.");
                    System.out.println("1 - View a recipe in database");
                    System.out.println("2 - Add a recipe to the database");
                    System.out.println("0 - Quit application");
                    System.out.print("Enter your choice: ");
                    menuChoice = input.nextInt();
               }
                catch (InputMismatchException e) {
                    menuChoice = 6;
                    input.next();
                }

               if (menuChoice == 1) {
                   viewRecipes(entityManager, transaction, input);
                   repeat = true;
               } else {
                   if (menuChoice == 2) {
                       addRecipe(entityManager, transaction, input);
                       repeat = true;
                  } else {
                       if (menuChoice == 0) {
                           System.out.println("Thank you!");
                           repeat = false;
                       } else {
                           System.out.println("Invalid Choice. Please try again.");
                          repeat = true;
                      }
                  }
               }
          }while (repeat);
          } finally {
               if(transaction.isActive()) {
                  transaction.rollback();
              }
              entityManager.close();
              entityManagerFactory.close();
        }
    }

    public static void addRecipe(EntityManager entityManager,
                                 EntityTransaction transaction, Scanner input) {

        int recipeId;
        String recipeName;
        String recipeIngredients;
        String recipeMethod;
        int dayPlanned;

        System.out.print("Type in the Recipe ID (Number): ");
        recipeId = input.nextInt();
        input.nextLine();
        System.out.print("Type in the Recipe Name (Text): ");
        recipeName = input.nextLine();
        System.out.print("Type in the Recipe Ingredients (Text): ");
        recipeIngredients = input.nextLine();
        System.out.print("Type in the Preparation Method (Text): ");
        recipeMethod = input.nextLine();
        System.out.print("Type in the day you plan on preparing this recipe (Number): ");
        dayPlanned = input.nextInt();
        input.nextLine();

        Recipe recipe = new Recipe();

        try {
            transaction.begin();

            recipe.setIdrecipeList(recipeId);
            recipe.setRecipeName(recipeName);
            recipe.setRecipeIngredients(recipeIngredients);
            recipe.setPrepMethod(recipeMethod);
            recipe.setDayPlanned(dayPlanned);
            entityManager.persist(recipe);

            transaction.commit();
            System.out.println("Recipe added successfully. Returning to main menu...");
        }catch (JDBCException e) {
            System.out.println("There was a problem adding your recipe. Please try again.");
        }
    }

    public static void viewRecipes(EntityManager entityManager,
                                   EntityTransaction transaction, Scanner input){

        int recipeID;
        try {
            System.out.print("Type the ID of the recipe you want to see: ");
            recipeID = input.nextInt();

            try {
                transaction.begin();
                String sql = "FROM Recipe WHERE idrecipeList=" + Integer.toString(recipeID);
                Recipe recipe = (Recipe) entityManager.createQuery(sql).getSingleResult();
                transaction.commit();
                System.out.println(recipe.toString());
            }catch (Exception e) {
                e.printStackTrace();
                transaction.rollback();
            }
        }catch (InputMismatchException e) {
            System.out.println("Type a number.");
            input.next();
        }
        System.out.println("You are being redirected to the main menu...\n\n");
    }
}
