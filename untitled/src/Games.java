public class Games {
    public String console;
    public String title;
    public String played;

    public Games(String console, String title, String played){
        this.console = console;
        this.title = title;
        this.played = played;
    }

    @Override
    public String toString() {
        return "Title: " + title + ". Console: " + console + ". Played? " + played + ".";
    }
}