import java.util.*;

public class main {
    public static void main(String[] args) {
        try {
            System.out.println("\n---List---");
            List newList = new ArrayList();
            newList.add("Milk");
            newList.add("Bread");
            newList.add("Toothpaste");
            newList.add("Eggs");

            for (Object str : newList) {
                System.out.println((String) str);
            }

            System.out.println("\n---Queue---");
            Queue q = new PriorityQueue();
            q.add("1");
            q.add("2");
            q.add("3");
            q.add("4");
            Iterator i = q.iterator();
            while (i.hasNext()) {
                System.out.println(q.poll());
            }

            System.out.println("\n---Set---");
            Set set = new TreeSet();
            set.add("a");
            set.add("b");
            set.add("c");
            set.add("d");
            set.add("e");
            for (Object str : set) {
                System.out.println((String) str);
            }
            System.out.println("\n---Generics---");
            List<Games> games = new LinkedList<Games>();
            games.add((new Games("Xbox", "DOOM Eternal", "No")));
            games.add((new Games("PC", "GTA V", "Yes")));
            games.add((new Games("PS5", "COD: Warzone", "No")));
            for (Games gam : games) {
                System.out.println(gam);
            }
            System.out.println("\n---Compare---");
            Collections.sort(games, new Sortbyname());
            System.out.println("Sorted by Title Name");
            for (int j = 0; j < games.size(); j++) {
                System.out.println(games.get(j));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
class Sortbyname implements Comparator<Games>{

    @Override
    public int compare(Games a, Games b) {
        return a.title.compareTo(b.title);
    }
}
