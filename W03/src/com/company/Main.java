package com.company;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        int num1 = 0;
        int num2 = 0;
        Scanner input = new Scanner(System.in);

        do {
            System.out.print("Type the first number: ");
            num1 = input.nextInt();

            System.out.print("Type the second number: ");
            num2 = input.nextInt();

            if(num2==0)
                System.out.println("The second number cannot be zero. Please try again. ");
        }while(num2==0);

        System.out.println("The result of the division is: " + doDivision(num1,num2));
        input.close();
    }

    static float doDivision(int num1, int num2) {
            return (float) num1/num2;
    }
}