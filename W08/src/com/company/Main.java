package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        foodOrder ds1 = new foodOrder("Bob", 1, "Cheeseburger", 100);
        foodOrder ds2 = new foodOrder("Sally", 2, "Tacos", 800);
        foodOrder ds3 = new foodOrder("Billy", 3, "Sushi", 120);
        foodOrder ds4 = new foodOrder("Anna", 4, "Pizza", 600);
        foodOrder ds5 = new foodOrder("Pat", 5, "Chicken Sandwich", 300);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();

    }
}
