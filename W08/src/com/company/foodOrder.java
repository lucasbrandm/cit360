package com.company;

import java.util.Random;

public class foodOrder implements Runnable {

    private int orderNumber;
    private String order;
    private String custName;
    private int sleep;
    private int rand;

    public foodOrder(String custName, int orderNumber, String order, int sleep) {

        this.custName = custName;
        this.orderNumber = orderNumber;
        this.sleep = sleep;
        this.order = order;

        Random random = new Random();
        this.rand = random.nextInt(5);
    }

    public void run() {
        System.out.println("\n\nPreparing order# " + orderNumber + " for customer: " + custName);
        for (int count = 1; count < rand; count++) {
            if (count % orderNumber == 0) {
                System.out.print("Order# " + orderNumber + " (" + order + ") is waiting to leave.\n\n");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("Order Number: " + orderNumber + " out for delivery..");
    }
}
