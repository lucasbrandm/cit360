package com.example.W07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        boolean validUser = false;
        boolean validPass = false;
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String username = null;
        try {
            username = request.getParameter("username");
            if (username.length() < 3) {
                out.println("<p>Username entered is too short.</p>");
                validUser = false;
            } else if (username.length() > 20) {
                out.println("<p>Username entered is too long.</p>");
                validUser = false;
            } else {
                validUser = true;
            }
        } catch (Exception e) {
            out.println("<p>Invalid Response. Please login again</p>");
        }
        String password = null;
        try {
            password = request.getParameter("password");
            if (password.length() < 3) {
                out.println("<p>Your password is too short.</p>");
                validPass = false;
            } else if (password.length() > 20) {
                out.println("<p>Your password is too long.</p>");
                validPass = false;
            } else {
                validPass = true;
            }
        } catch (Exception e) {
            out.println("<p>Invalid Response. Please login again</p>");
        }
        if ((validUser != true) || (validPass != true)) {
            out.println("<p>Please login again</p>");
            out.println("<a href=\"/W07_war_exploded/\">Click here to go back<a>");
        } else {
            out.println("<h1>Welcome to BookFace!</h1>");
            out.println("<h3>Login Successful</h3>");
            out.println("<p>Welcome   " + username + "</p>");
            out.println("<p>We're sorry, but there was a breach and your password (" + password + ") has been leaked.</p>");
            out.println("</body></html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("404 - Page not found");
    }
}
