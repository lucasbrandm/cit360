package com.company;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;


public class Main {

    public static void main(String[] args) {
    }

    @Test
    public void testAssertEquals() {
        String name1 = "James";
        String name2 = "James";
        assertEquals(name1, name2);
    }

    @Test
    public void testAssertSame() {
        String name1 = "James";
        String name2 = "John";
        assertSame(name1, name2);
    }

    //This might be lazy, but these three tests are really similar.
    @Test
    public void testAssertNotSame() {
        String name1 = "James";
        String name2 = "John";
        assertNotSame(name1, name2);
    }


    @Test
    public void testAssertThat() {
        int answerToLife = 42;
        //assertThat has been deprecated!!
        assertThat(answerToLife,is(42));
    }

    @Test
    public void testAssertNull() {
        String name1 = "James";
        String name2 = null;
        assertNull(name2);
        assertNotNull(name1);
    }

    @Test
    public void testAssertTrueAndFalse() {
        boolean bool1 = true;
        boolean bool2 = false;
        assertTrue(bool1);
        assertFalse(bool2);
    }

    @Test
    public void testAssertArrayEquals(){
        int [] array1 = new int[] {1, 2, 3, 4};
        int [] array2 = new int[] {1, 3, 2, 6};
        assertArrayEquals(array1, array2);
    }
}
