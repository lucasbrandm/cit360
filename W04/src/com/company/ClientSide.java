package com.company;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ClientSide {

    public static void main(String[] args) {
        try {
            Joke joke = new Joke();
            joke.setSetup("Where do you learn to make banana splits?");
            joke.setPunchline("At sundae school.");

            HTTPServer server = new HTTPServer(joke);
            String json = getContent("http://localhost/json");
            System.out.println("--Printing the JSON to check--");
            System.out.println(json);

            System.out.println("--Printing the JSON turned into Joke--");
            System.out.println(JSONtoJoke(json));

        } catch (IllegalArgumentException | IOException e){
            System.out.println(e.toString());
        }
    }

    public static String getContent(String urlstring){
        String content = "";
        try {
            URL url = new URL(urlstring);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null){
                stringBuilder.append((line + "\n"));
            }
            content = stringBuilder.toString();
        } catch (Exception e){
            System.err.println(e.toString());
        }
        return content;
    }

    public static Joke JSONtoJoke(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Joke joke = null;
        joke =  mapper.readValue(json, Joke.class);
        return joke;
    }
}
