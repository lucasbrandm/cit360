package com.company;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTPServer {
    public HTTPServer(Joke joke) throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(80),0);
        httpServer.createContext("/json", new handler(joke));
        httpServer.setExecutor(null);
        httpServer.start();
    }

    private class handler implements HttpHandler {
        private Joke _joke;

        // takes the student object and save it into a variable.
        public handler(Joke joke) {
            _joke = joke;
        }

        // convert student object into JSON and send it to the header so the client can get the JSON created
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String response = jokeToJSOn(_joke);
            exchange.sendResponseHeaders(200, response.length());
            exchange.getResponseHeaders().set("Content-Type", "application/json");
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();

        }
    }

    public String jokeToJSOn(Joke joke) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String s = null;
        s = mapper.writeValueAsString(joke);

        return s;
    }
}
