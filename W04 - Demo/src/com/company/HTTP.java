package com.company;

import java.io.*;
import java.net.*;

public class HTTP {
    public static String getContent(String urlstring){
        String content = "";
        try {
            URL url = new URL(urlstring);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null){
                stringBuilder.append((line + "\n"));
            }
            content = stringBuilder.toString();
        } catch (Exception e){
            System.err.println(e.toString());
        }
        return content;
    }


}
