package com.company;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSON {
    public static Joke JSONtoJoke(String jsonstring){
        ObjectMapper mapper = new ObjectMapper();
        Joke joke = null;
        try{
            joke = mapper.readValue(jsonstring, Joke.class);
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return joke;
    }


}
